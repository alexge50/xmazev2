#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;

out vec2 TexCoords0;
out vec3 Normal0;
out vec3 Position0;

//uniform mat4 model;
//uniform mat4 view;
//uniform mat4 projection;

uniform mat4 MVP;

void main()
{   
    gl_Position = /*projection * view * model **/  MVP * vec4(position, 1.0f);
    TexCoords0 = texCoords;
    Normal0 = normal;
    Position0 =  (MVP * vec4(position, 1.0f)).xyz;
}
