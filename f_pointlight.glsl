#version 330


struct  Attenuation{
        float Constant;
        float Linear;
        float Exp;
    };

struct PointLight
{
    vec3 Color;
    float AmbientIntensity;
    float DiffuseIntensity;
    vec3 Position;
    
    Attenuation Atten;
};

//gbuffer data
uniform sampler2D PositionMap;
uniform sampler2D ColorMap;
uniform sampler2D NormalMap;

//light data
uniform PointLight pointlight;

//other
uniform vec3 EyeWorldPos;
uniform float MatSpecularIntensity;
uniform float SpecularPower;
uniform vec2 ScreenSize;



vec4 calcLightInternal(PointLight pl, vec3 LightDirection, vec3 Position, vec3 Normal)
{
	vec4 amb_color=vec4(pl.Color,1.0)*pl.AmbientIntensity;
	float diff_factor=dot(Normal,-LightDirection);
   
    vec4 diff_color=vec4(0,0,0,0);
    vec4 spec_color=vec4(0,0,0,0);
  
    if(diff_factor>0.0)
    {
		diff_color=vec4(pl.Color,1.0)*pl.DiffuseIntensity*diff_factor;
		
		vec3 VertexToEye =normalize(EyeWorldPos-Position);
		vec3 LightReflect=normalize(reflect(LightDirection,Normal));

		float spec_factor=dot(VertexToEye,LightReflect);
		spec_factor=pow(spec_factor,SpecularPower);
		

		if(spec_factor>0.0)
		{
			spec_color=vec4(pl.Color,1.0)*MatSpecularIntensity*spec_factor;
		}
    }
	
	
	return  (amb_color+diff_color+spec_color);
}


vec4 calcPointLight(vec3 Position, vec3 Normal)
{
	vec3 LightDirection=Position-pointlight.Position;
	float Distance=length(LightDirection);
	LightDirection=normalize(LightDirection);
	
	vec4 color=calcLightInternal(pointlight,LightDirection,Position,Normal);
	
	float Atten=pointlight.Atten.Constant+
				pointlight.Atten.Linear*Distance+
				pointlight.Atten.Exp*Distance*Distance;
	
	//Atten=max(1.0,Atten);
	//return color/Atten;
	return color;
}


vec2 gbufferTexCoord()
{
    return gl_FragCoord.xy/ScreenSize;
}

out vec4 FragColor;

void main()
{
    vec2 gbuffer_texcoord=gbufferTexCoord();
    
    vec3 Position=texture(PositionMap,gbuffer_texcoord).xyz;
    vec3 Color   =texture(ColorMap   ,gbuffer_texcoord).xyz;
    vec3 Normal  =texture(NormalMap  ,gbuffer_texcoord).xyz;

	FragColor=vec4(Color,1.0)*calcPointLight(Position,Normal);
}
