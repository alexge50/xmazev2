#include "GLProgram.h"
#include "GBuffer.h"
#include "camera.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <SOIL/SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "logl-camera.hpp"
#include  "game_logic.h"

#define SCREEN_W 1366
#define SCREEN_H 768

#include "Model.h"
#include "FT_text.h"

#include <unistd.h>
//#pragma once

using namespace glm;

#define my_max(a,b) ((a)>(b)?(a):(b))

GLFWwindow* window;



struct
{
    PointLight_Params monster,laser,portal,entity;
} light_template;

GLuint screenWidth = 1920, screenHeight = 1080;
char keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;


Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();

float CalcPointLightBSphere(const PointLight_Params Light);//distance scale for a point light


void dbg()
{
    printf("here\n");
}

mat4 mat4_InitIdentity();
mat4 mat4_myView();
vec3 vec3_myPos();

int main(void)
{

    GLSLProgram prog;
    GLuint alpha_map;
    GLint texloc;
    PointLight_Params template_pointlight;
    DirectionalLight_Params m_dirLight;
    PL_UniformLocations u_locs;
    DL_UniformLocations u_locsd;
    GLSLProgram pointlight_pass;
    GLSLProgram dirlight_pass;
    GLSLProgram text_pass;
    int i;
    GLuint u_eyeworldpos;
    GLuint u_matspec;
    GLuint u_specpow;
    GLuint u_screensize;
    GLuint u_position;
    GLuint u_color;
    GLuint u_normal;

    GLuint du_eyeworldpos;
    GLuint du_matspec;
    GLuint du_specpow;
    GLuint du_screensize;
    GLuint du_position;
    GLuint du_color;
    GLuint du_normal;

    xmaze::logic logic;

    xText arial;

    //glfwSetErrorCallback(error_callback);

    if (!glfwInit())
        exit(EXIT_FAILURE);
    window = glfwCreateWindow(SCREEN_W, SCREEN_H, "Simple example", glfwGetPrimaryMonitor(), NULL);

    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    glslInit();

    glViewport(0,0,SCREEN_W,SCREEN_H);
    //glEnable(GL_TEXTURE_2D);
    if(1){
        glClear(GL_COLOR_BUFFER_BIT);

        GLfloat points[]={
            -1,-1,0,  0,0,
            -1,1,0,   0,1,
            1,1,0,    1,1,
            1,-1,0,   1,0
        };
        GLuint t=SOIL_load_OGL_texture("Data/Title.png",SOIL_LOAD_AUTO,
                                       SOIL_CREATE_NEW_ID,
                                       SOIL_FLAG_INVERT_Y);
        GLSLProgram s=glslBuildProgram("v_title.glsl","f_title.glsl");




        mat4 mvp=ortho(-1,1,-1,1,-1,1);






        GLuint VBO, VAO;
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);

        glBindVertexArray(VAO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

        // Position attribute
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);
         //TexCoord attribute
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(1);

        glBindVertexArray(0);

        glslUse(s);
        glUniformMatrix4fv(glGetUniformLocation(s.id,"mvp"), 1, GL_FALSE, glm::value_ptr(mvp));
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D,t);
        glUniform1i(glGetUniformLocation(s.id,"_tex"),0);

        glBindVertexArray(VAO);
        glDrawArrays(GL_QUADS,0,4);
        glBindVertexArray(0);

        glfwSwapBuffers(window);
    }


    //glDisable();

    prog=glslBuildProgram("v_gbuffer.glsl","f_gbuffer.glsl");
    pointlight_pass=glslBuildProgram("v_pointlight.glsl","f_pointlight.glsl");
    dirlight_pass  =glslBuildProgram("v_dirlight.glsl","f_dirlight.glsl");
    text_pass      =glslBuildProgram("v_text.glsl","f_text.glsl");



    GLuint MatrixID = glGetUniformLocation(prog.id, "MVP");
    PointLight pl;
    DirectionalLight dl;


    u_locs.mvp=glGetUniformLocation(pointlight_pass.id,"MVP");
    u_locs.color=glGetUniformLocation(pointlight_pass.id,"pointlight.Color");
    u_locs.amb_intens=glGetUniformLocation(pointlight_pass.id,"pointlight.AmbientIntensity");
    u_locs.diff_intens=glGetUniformLocation(pointlight_pass.id,"pointlight.DiffuseIntensity");
    u_locs.position=glGetUniformLocation(pointlight_pass.id,"pointlight.Position");
    u_locs.atten_const=glGetUniformLocation(pointlight_pass.id,"pointlight.Atten.Constant");
    u_locs.atten_lin=glGetUniformLocation(pointlight_pass.id,"pointlight.Atten.Linear");
    u_locs.atten_exp=glGetUniformLocation(pointlight_pass.id,"pointlight.Atten.Exp");

    u_eyeworldpos   =glGetUniformLocation(pointlight_pass.id,"EyeWorldPos");
    u_matspec       =glGetUniformLocation(pointlight_pass.id,"MatSpecularIntensity");
    u_specpow       =glGetUniformLocation(pointlight_pass.id,"SpecularPower");
    u_screensize    =glGetUniformLocation(pointlight_pass.id,"ScreenSize");
    u_position      =glGetUniformLocation(pointlight_pass.id,"PositionMap");
    u_color         =glGetUniformLocation(pointlight_pass.id,"ColorMap");
    u_normal        =glGetUniformLocation(pointlight_pass.id,"NormalMap");


    u_locsd.mvp=glGetUniformLocation(dirlight_pass.id,"MVP");
    u_locsd.color=glGetUniformLocation(dirlight_pass.id,"dirlight.Color");
    u_locsd.amb_intens=glGetUniformLocation(dirlight_pass.id,"dirlight.AmbientIntensity");
    u_locsd.diff_intens=glGetUniformLocation(dirlight_pass.id,"dirlight.DiffuseIntensity");
    u_locsd.direction=glGetUniformLocation(dirlight_pass.id,"dirlight.Direction");

    du_eyeworldpos   =glGetUniformLocation(dirlight_pass.id,"EyeWorldPos");
    du_matspec       =glGetUniformLocation(dirlight_pass.id,"MatSpecularIntensity");
    du_specpow       =glGetUniformLocation(dirlight_pass.id,"SpecularPower");
    du_screensize    =glGetUniformLocation(dirlight_pass.id,"ScreenSize");
    du_position      =glGetUniformLocation(dirlight_pass.id,"PositionMap");
    du_color         =glGetUniformLocation(dirlight_pass.id,"ColorMap");
    du_normal        =glGetUniformLocation(dirlight_pass.id,"NormalMap");




    Model nanosuit("Data/nanosuit/nanosuit.obj");
    Model sphere("Data/sphere/sphere.obj");
    Model box("Data/cube/cube.obj");
    Model quad("Data/quad/untitled.obj");
    Model Wall("Data/Wall/box.obj");
    Model Floor("Data/Floor/plane.obj");
    Model monster("Data/monster/monster.obj");
    Model laser("Data/laser/laser.obj");
    arial=xText("Data/fonts/arial.ttf",0,48);

    GBuffer buff;
    buff.init(SCREEN_W,SCREEN_H);
    dbg();

    pl.initModel(sphere);
    pl.init(u_locs);
    dl.initModel(quad);
    dl.init(u_locsd);

    //init light templates
    light_template.monster.DiffuseIntensity = 0.8f;
    light_template.monster.AmbientIntensity = 0.5f;
    //light_template.monster.Color = vec3(0.,1.,0.);
    //light_template.monster.Position = vec3(1.0f, 1.5f, 5.0f);
    light_template.monster.Atten.Constant = 0.1f;
    light_template.monster.Atten.Linear = 0.2f;
    light_template.monster.Atten.Exp = 0.4f;


    light_template.laser.DiffuseIntensity = 0.8f;
    light_template.laser.AmbientIntensity = 0.5f;
    light_template.laser.Color = vec3(1.,0.,0.);
    //light_template.laser.Position = vec3(1.0f, 1.5f, 5.0f);
    light_template.laser.Atten.Constant = 0.1f;
    light_template.laser.Atten.Linear = 0.2f;
    light_template.laser.Atten.Exp = 0.2f;


    light_template.portal.DiffuseIntensity = 0.8f;
    light_template.portal.AmbientIntensity = 0.5f;
    light_template.portal.Color = vec3(1.,0.,1.);
    //light_template.laser.Position = vec3(1.0f, 1.5f, 5.0f);
    light_template.portal.Atten.Constant = 0.1f;
    light_template.portal.Atten.Linear = 0.2f;
    light_template.portal.Atten.Exp = 0.2f;


    light_template.entity.DiffuseIntensity = 0.8f;
    light_template.entity.AmbientIntensity = 0.5f;
    light_template.entity.Color = vec3(1.,1.,1.);
    //light_template.laser.Position = vec3(1.0f, 1.5f, 5.0f);
    light_template.entity.Atten.Constant = 0.1f;
    light_template.entity.Atten.Linear = 0.2f;
    light_template.entity.Atten.Exp = 0.4f;


    //directional light
    m_dirLight.AmbientIntensity = 0.5f;
    m_dirLight.Color = vec3(1,1,1);
    m_dirLight.DiffuseIntensity = 0.6f;
    m_dirLight.Direction = vec3(1.0f, .0f, .0f);

    dl.set_LightData(m_dirLight);
    GLuint texture_box=SOIL_load_OGL_texture("Data/box.png",SOIL_LOAD_AUTO,
                       SOIL_CREATE_NEW_ID,
                       SOIL_FLAG_INVERT_Y);
    GLuint portal_box=SOIL_load_OGL_texture("Data/portal.png",SOIL_LOAD_AUTO,
                                            SOIL_CREATE_NEW_ID,
                                            SOIL_FLAG_INVERT_Y);

    //sleep(3);

    //glEnable(GL_TEXTURE_2D);
    float rota;
    int clock=0;
    int lvl=1;
    char isexit=0;

    sleep(5);

    while (!glfwWindowShouldClose(window))
    {
        clock++;

        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // Check and call events
        glfwPollEvents();
        Do_Movement();


        if(logic.isDone())
        {
            if(lvl<=6)
            {
                char str[7];
                sprintf(str,"Data/levels/level%d",lvl);
                logic.newlevel(str);
                lvl++;
            }
            else isexit=1;
        }

        if(isexit||!logic.getLife())
        {
            glfwSetWindowShouldClose(window, GL_TRUE);
        }

        if(clock%2==0)
            logic.update(keys);

        //"geometry pass"
        glEnable(GL_DEPTH_TEST);
        glDepthMask(GL_TRUE);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        buff.bind_write();

        //glm::mat4 model=glm::mat4(1.f);
        glm::mat4 VP = glm::perspective(45.0f,(float)SCREEN_W/(float)SCREEN_H,0.1f,1000.f)*mat4_myView();
        //glm::mat4 view=camera.GetViewMatrix();
        //glm::mat4 VP = glm::perspective(45.0f,(float)SCREEN_W/(float)SCREEN_H,0.1f,1000.f)*view;


//         printf("\n\n\n\n\n\n\n%f %f %f\n",camera.Position[0],camera.Position[1],camera.Position[2]);
        int i,j;
//        for(i=0;i<4;i++)
//        {
//            for(j=0;j<4;j++)
//            printf("%f ",view[i][j]);
//            printf("\n");
//        }


        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glslUse(prog);

        xmaze::point portals[40*22],lasers[40*22];
        int n_portals=0,n_lasers=0;

        for(i=0; i<TABLE_WIDTH; i++)
        {
            for(j=0; j<TABLE_HEIGHT; j++)
            {

                //printf("%c",logic.getInfo(j,i));
                if(logic.getInfo(j,i)=='#'||logic.getInfo(j,i)=='@')
                {
                    mat4 MVP;
                    mat4 model;

                    model=translate(model,vec3((float)0.f,(float)j,(float)i));
                    //model=scale(model,vec3(0.85f,0.85f,1.f));
                    MVP=VP*model;


                    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, glm::value_ptr(MVP));
                    glActiveTexture(GL_TEXTURE0);
                    if(logic.getInfo(j,i)=='#')
                        glBindTexture(GL_TEXTURE_2D,texture_box);
                    else
                        glBindTexture(GL_TEXTURE_2D,portal_box);
                    glUniform1i(glGetUniformLocation(prog.id,"texture_diffuse1"),0);

                    Wall.draw(prog);

                    if(logic.getInfo(j,i)=='@')
                    {
                        portals[n_portals]=xmaze::point(j,i);
                        n_portals++;
                    }

                }
                else
                {
                    mat4 MVP;
                    mat4 model;

                    model=translate(model,vec3((float)0.f,(float)j,(float)i));
                    //model=rotate(model,(float)60.,vec3(0.f,0.f,-1.f));
                    //model=scale(model,vec3(1.f,4.f,4.f));
                    MVP=VP*model;


                    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, glm::value_ptr(MVP));
                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D,texture_box);
                    glUniform1i(glGetUniformLocation(prog.id,"texture_diffuse1"),0);

                    Floor.draw(prog);
                    if(logic.getInfo(j,i)=='>'||
                            logic.getInfo(j,i)=='V'||
                            logic.getInfo(j,i)=='^'||
                            logic.getInfo(j,i)=='<')
                    {
                        mat4 MVP;
                        mat4 model;
                        float rot[128];

                        rot['>']=0;
                        rot['V']=90;
                        rot['<']=180;
                        rot['^']=270;

                        model=translate(model,vec3((float)0.f,(float)j+0.707f,(float)i-0.707f));
                        model=rotate(model,radians(rot[logic.getInfo(j,i)]),vec3(1.0f,0.0f,0.0f));
                        MVP=VP*model;
                        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, glm::value_ptr(MVP));


                        nanosuit.draw(prog);

                        //rota+=0.1;
                    }
                    else if(logic.getInfo(j,i)=='M'||
                            logic.getInfo(j,i)=='L')
                    {
                        mat4 MVP;
                        mat4 model;
                        float rot[128];
                        char m[3][3];
                        char d;
                        int dl,dc;

                        rot['>']=0;
                        rot['V']=90;
                        rot['<']=180;
                        rot['^']=270;

                        m[ 1+1][ 0+1]='^';
                        m[-1+1][ 0+1]='V';
                        m[ 0+1][-1+1]='<';
                        m[ 0+1][ 1+1]='>';

                        logic.getMonsterOrientation(&dl,&dc,j,i);
                        d=m[dl+1][dc+1];

                        model=translate(model,vec3((float)0.f,(float)j+0.707f,(float)i-0.707f));
                        model=rotate(model,radians(rot[d]),vec3(1.0f,0.0f,0.0f));
                        MVP=VP*model;
                        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, glm::value_ptr(MVP));


                        monster.draw(prog);
                    }
                    else if(logic.getInfo(j,i)=='X')
                    {
                        mat4 MVP;
                        mat4 model;

                        model=translate(model,vec3((float)0.f,(float)j+0.707f,(float)i-0.707f));
                        //model=scale(model,vec3(0.85f,0.85f,1.f));
                        MVP=VP*model;
                        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, glm::value_ptr(MVP));

                        laser.draw(prog);

                        lasers[n_lasers]=xmaze::point(j,i);
                        n_lasers++;

                    }
                }
            }
            //printf("\n");
        }
        //printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");


        glDepthMask(GL_FALSE);
        glDisable(GL_DEPTH_TEST);

        //"light pass"
        glEnable(GL_BLEND);
        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_ONE,GL_ONE);

        buff.bind_read();
        glClear(GL_COLOR_BUFFER_BIT);

        glslUse(pointlight_pass);

        vec2  screen_size=vec2((float)SCREEN_W,(float)SCREEN_H);


        glUniform3fv(u_eyeworldpos,1,glm::value_ptr(vec3_myPos()));
        glUniform1f(u_matspec,0.);
        glUniform1f(u_specpow,0.);
        glUniform2fv(u_screensize,1,glm::value_ptr(screen_size));
        glUniform1i(u_position,       GBuffer_position);
        glUniform1i(u_color,          GBuffer_diffuse);
        glUniform1i(u_normal,         GBuffer_normal);


        for(i=0; i<logic.n_lights(); i++)//monstrii luminosi
        {
            xmaze::Light l;


            l=logic.light(i);
            if(l.active==1)
            {
                light_template.monster.Position=vec3(1.f,(float)l.l+2*0.707f,(float)l.c-2*0.707f);
                light_template.monster.Color=vec3(l.r,l.g,l.b);

                pl.set_LightData(light_template.monster);

                float _scale=CalcPointLightBSphere(pl.this_light);
                mat4 model;
                model=translate(model,pl.this_light.Position);
                model=scale(model,vec3(_scale,_scale,_scale));
                mat4 MVP=VP*model;

                pl.set_MVP(MVP);
                pl.render(pointlight_pass);
            }
        }

        for(i=0; i<n_lasers; i++)//lasere
        {


            light_template.laser.Position=vec3(1.f,(float)lasers[i].l/*+4*0.707f*/,(float)lasers[i].c/*-0.2f*/);

            pl.set_LightData(light_template.laser);

            float _scale=CalcPointLightBSphere(pl.this_light);
            mat4 model;
            model=translate(model,pl.this_light.Position);
            model=scale(model,vec3(_scale,_scale,_scale));
            mat4 MVP=VP*model;

            pl.set_MVP(MVP);
            pl.render(pointlight_pass);
        }

        for(i=0; i<n_portals; i++)//portale
        {


            light_template.portal.Position=vec3(1.f,(float)portals[i].l/*+3*0.707f*/,(float)portals[i].c/*-3*0.707f*/);

            pl.set_LightData(light_template.portal);

            float _scale=CalcPointLightBSphere(pl.this_light);
            mat4 model;
            model=translate(model,pl.this_light.Position);
            model=scale(model,vec3(_scale,_scale,_scale));
            mat4 MVP=VP*model;

            pl.set_MVP(MVP);
            pl.render(pointlight_pass);
        }
        if(!logic.isDirectional)
        {
            light_template.entity.Position=vec3(1.f,(float)logic.entity.l+2*0.707f,(float)logic.entity.c-2*0.707f);

            pl.set_LightData(light_template.entity);

            float _scale=CalcPointLightBSphere(pl.this_light);
            mat4 model;
            model=translate(model,pl.this_light.Position);
            model=scale(model,vec3(_scale,_scale,_scale));
            mat4 MVP=VP*model;

            pl.set_MVP(MVP);
            pl.render(pointlight_pass);
        }



        glslUse(dirlight_pass);
        glUniform3fv(du_eyeworldpos,1,glm::value_ptr(vec3_myPos()));
        glUniform1f(du_matspec,0.);
        glUniform1f(du_specpow,0.);
        glUniform2fv(u_screensize,1,glm::value_ptr(screen_size));
        glUniform1i(u_position,       GBuffer_position);
        glUniform1i(u_color,          GBuffer_diffuse);
        glUniform1i(u_normal,         GBuffer_normal);
        for(i=0; i<1; i++)
        {
            mat4 MVP;
            MVP=mat4_InitIdentity();
            dl.set_MVP(MVP);
            if(logic.isDirectional)
                dl.render(dirlight_pass);
        }
        glDisable(GL_BLEND);



        glEnable(GL_CULL_FACE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        mat4 MVP= glm::ortho(0.0f, (float)SCREEN_W, 0.0f, (float)SCREEN_H);

        char str[14];

        sprintf(str,"Life: %d",logic.getLife());

        arial.print(text_pass,MVP,str,22.0f, 22.0f, 1.0f, glm::vec3(0.9f, 0.1f, 0.1f));
        arial.print(text_pass,MVP,str,25.0f, 25.0f, 1.0f, glm::vec3(0.5f, 0.8f, 0.2f));


        sprintf(str,"Score: %6d",logic.getScore());

        arial.print(text_pass,MVP,str,997.0f, 22.0f, 1.0f, glm::vec3(0.9f, 0.1f, 0.1f));
        arial.print(text_pass,MVP,str,1000.f, 25.0f, 1.0f, glm::vec3(0.5f, 0.8f, 0.2f));



        arial.print(text_pass,MVP,"Xmaze",597.0f, 697.0f, 1.0f, glm::vec3(0.0f, 0.1f, 0.9f));
        arial.print(text_pass,MVP,"Xmaze",600.0f, 700.0f, 1.0f, glm::vec3(0.0f, 0.9, 0.9f));

        glDisable(GL_CULL_FACE);
        glDisable(GL_BLEND);


        glfwSwapBuffers(window);
        glfwPollEvents();

    }

    //sleep(2);

    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}



mat4 mat4_myView()
{
    /*
    mat4 m;
    m[0][0]=0.999848;   m[0][1]=-0.008738;  m[0][2]=-0.015057;  m[0][3]=0.000000;
    m[1][0]=0.000001;   m[1][1]=0.864911;   m[1][2]=-0.501926;  m[1][3]=0.000000;
    m[2][0]=-0.501926;  m[2][1]=-0.501926;  m[2][2]=0.864780;   m[2][3]=0.000000;
    m[3][0]=-57.450260; m[3][1]=-19.704458; m[3][2]=-57.989388; m[3][3]=1.000000;
    */
    /*mat4 m(
        1.000000, -0.000156, -0.000084, 0.000000,
        0.000001, 0.478210, -0.878246, 0.000000,
        0.000177, 0.878246, 0.478210, 0.000000,
        -26.933001, -11.047264, -28.257359, 1.000000);
    */
    /*
    mat4 m(
        1.000000, -0.000077, -0.000538, 0.000000,
        0.000000, 0.989798, -0.142479, 0.000000,
        0.000543, 0.142479, 0.989798, 0.000000,
        -24.933285, -12.649275, -25.703405, 1.000000

    );*/

//    mat4 m(
//        0.020047, -0.346517, -0.937829, 0.000000,
//        -0.000000, 0.938018, -0.346587, 0.000000,
//        0.999799, 0.006948, 0.018805, 0.000000,
//        -19.574230, -8.253429, -20.822960, 1.000000
//
//    );


    mat4 m(
        0.017522, -0.520442, -0.853717, 0.000000,
        -0.000000, 0.853848, -0.520522, 0.000000,
        0.999847, 0.009121, 0.014961, 0.000000,
        -19.387497, -7.801500, -19.889874, 1.000000
    );

    return m;
}

vec3 vec3_myPos()
{
    /*
        vec3 v;
        v[0]= 56.396248;
        v[1]=-12.063748;
        v[2]= 61.036842;
     */
    //vec3 v(26.928907,-19.533962,23.219923);
    //vec3 v(24.918474,8.858030,27.256975);
    //vec3 v(-21.995932, 0.524890, 20.019213);
    vec3 v(-20.700842, -3.691829, 19.753252);

    return v;
}


mat4 mat4_InitIdentity()
{
    mat4 m;
    m[0][0] = 1.0f;
    m[0][1] = 0.0f;
    m[0][2] = 0.0f;
    m[0][3] = 0.0f;
    m[1][0] = 0.0f;
    m[1][1] = 1.0f;
    m[1][2] = 0.0f;
    m[1][3] = 0.0f;
    m[2][0] = 0.0f;
    m[2][1] = 0.0f;
    m[2][2] = 1.0f;
    m[2][3] = 0.0f;
    m[3][0] = 0.0f;
    m[3][1] = 0.0f;
    m[3][2] = 0.0f;
    m[3][3] = 1.0f;
    return m;
}



/*******Input Handeling*******/
void Do_Movement()
{
    // Camera controls
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    //cout << key << endl;
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(action == GLFW_PRESS)
        keys[key] = true;
    else if(action == GLFW_RELEASE)
        keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}




/**************/

float CalcPointLightBSphere(const PointLight_Params Light)//distance scale for a point light
{
    float MaxChannel = my_max(my_max(Light.Color.x, Light.Color.y), Light.Color.z);

    float ret = (-Light.Atten.Linear + sqrtf(Light.Atten.Linear * Light.Atten.Linear -
                 4 * Light.Atten.Exp * (Light.Atten.Exp - 256 * MaxChannel * Light.DiffuseIntensity)))
                /
                2 * Light.Atten.Exp;
    return ret;
}
