#include <stdio.h>
#include <stdlib.h>

#define TABLE_WIDTH 40
#define TABLE_HEIGHT 22
#define DEPTH 7

#define SPACE ' '

namespace xmaze
{

struct point
{
    int l,c;
    point();
    point(int _l,int _c);
    void setpoint(int _l,int _c);
};

struct character: point
{
    char p_init;
};


struct Light: point
{
    Light();
    Light(float r,float g,float b,int l,int c);
    float r,g,b;
    char active;
};

class logic
{

public:
    logic();
    char getInfo(int l, int c);
    void update(char *keys);
    void newlevel(char* fname);
    int getLife();
    int getScore();
    void getMonsterOrientation(int*l,int*c,int l_now,int c_now);
    char isDone();
    Light light(int i);
    int n_lights();

    char isDirectional;
    character entity;
private:
    Light* light_index;
    int v_n_lights;
    char table[TABLE_HEIGHT][TABLE_WIDTH][DEPTH];
    int life;
    int score;
    char clock;
    //long long int clock2;
    character character_start;

    char done;
    void loadlevel(char*);
   // void restartlevel();
};

};
