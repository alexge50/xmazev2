#include <GLFW/glfw3.h>
#include <math.h>
#include <stdio.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


//using namespace glm;

struct XCamera
{
    GLFWwindow *window;

    struct matrix_s
    {
        glm::mat4 view;
        glm::mat4 projection;
    }matrix;

    glm::vec3 pos;

    struct
    {
        float x,y;
        float xc;
    }angle;

    float fov;


    struct
    {
        float move,look;
    }speed;
    long long lastUpdateTime;
    struct
    {
        float width;
        float height;
        float ratio;
    }window_size;
};


void xcamInit(XCamera* cam,GLFWwindow *window,float width,float height);

XCamera::matrix_s xcamGetMatrix(XCamera cam);

void xcamUpdate(XCamera* cam,GLFWwindow *window,float width,float height);

void xcamSetWindow(XCamera* cam,GLFWwindow*,float width,float height);

void xcamUpdateLook(XCamera* cam,double x_offset,double y_offset);

void xcamUpdatePos(XCamera* cam,char key);
