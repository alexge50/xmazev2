#include "GBuffer.h"

#include <stdio.h>
#include <stdlib.h>


GBuffer::GBuffer()
{
    id_fbo=0;
    id_depthtex=0;
    ZERO_MEM(id_tex);
}

GBuffer::~GBuffer()
{
    if (id_fbo != 0)
    {
        glDeleteFramebuffers(1, &id_fbo);
    }

    if (id_tex[0] != 0)
    {
        glDeleteTextures(GBUFFER_TEXTURE_NUM, id_tex);
    }

    if (id_depthtex != 0)
    {
	glDeleteTextures(1, &id_depthtex);
    }
}

char GBuffer::init(int width,int height)
{
    unsigned int i;

    glGenFramebuffers(1,&id_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER,id_fbo);

    glGenTextures(GBUFFER_TEXTURE_NUM,id_tex);
    glGenTextures(1,&id_depthtex);

    for(i=0; i<GBUFFER_TEXTURE_NUM; i++)
    {
        glBindTexture(GL_TEXTURE_2D,id_tex[i]);
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGB32F,width,height,0,GL_RGB,GL_FLOAT,NULL);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,GL_COLOR_ATTACHMENT0+i,GL_TEXTURE_2D,id_tex[i],0);
    }

    glBindTexture(GL_TEXTURE_2D, id_depthtex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT,NULL);
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,id_depthtex, 0);

    GLenum DrawBuffers[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
    glDrawBuffers(4,DrawBuffers);

    GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (Status != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FB error, status: 0x%x\n", Status);
        return 0;
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    return 1;
}

void GBuffer::bind_write()
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER,id_fbo);
}

void GBuffer::bind_read()
{
    int i;
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
   // glBindFramebuffer(GL_READ_FRAMEBUFFER,id_fbo);


   for(i=0;i<GBUFFER_TEXTURE_NUM;i++)
   {
		glActiveTexture(GL_TEXTURE0+i);
		glBindTexture(GL_TEXTURE_2D,id_tex[i]);
   }

}

void GBuffer::set_read(GBuffer_texture_type TextureType)
{
    glReadBuffer(GL_COLOR_ATTACHMENT0 + TextureType);
}
