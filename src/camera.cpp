#include <sys/time.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "camera.h"
/*Tools*/
#define U2Stime(x) (x/1000000.)

inline long long getTime()
{
    struct timeval tv;

    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000000LL + tv.tv_usec;
}
/* */

using namespace glm;


void xcamInit(XCamera* _cam,GLFWwindow *window,float width,float height)
{
    XCamera cam;
    cam.pos=vec3(0.,0.,5.);
    cam.angle.x=3.14;
    cam.angle.xc=3.14;
    cam.angle.y=0;
    cam.fov=45;
    cam.speed.move=3;
    cam.speed.look=0.05;
    cam.window=window;
    cam.window_size.width=width;
    cam.window_size.height=height;
    cam.window_size.ratio=width/height;
    cam.lastUpdateTime=getTime();
    *_cam=cam;
}

void xcamSetWindow(XCamera* _cam,GLFWwindow *window,float width,float height)
{
    XCamera cam;
    cam.window_size.width=width;
    cam.window_size.height=height;
    cam.window_size.ratio=width/height;
    *_cam=cam;
}

XCamera::matrix_s xcamGetMatrix(XCamera cam)
{
    //cam.matrix.projection=glm::perspective(cam.fov,cam.window_size.ratio,0.1f,1000.f);
    //cam.matrix.view=glm::lookAt(cam.pos,cam.pos+dir,up);
    return cam.matrix;
}

void xcamUpdate(XCamera* _cam,GLFWwindow *window,float width,float height)
{
    XCamera cam;
    cam=*_cam;
    cam.window=window;
    cam.window_size.width=width;
    cam.window_size.height=height;
    cam.window_size.ratio=width/height;

    long long nowtime=getTime();
    float delta=U2Stime((nowtime-cam.lastUpdateTime));
    cam.lastUpdateTime=nowtime;
    //float delta =1;

    double xpos=0,ypos=0;
    glfwGetCursorPos(cam.window,&xpos,&ypos);

    glfwSetCursorPos(cam.window,cam.window_size.width/2.,cam.window_size.height/2.);

    //float updatey=1;
    float updatey=cam.speed.look*(cam.window_size.height/2.-ypos);

    //if(1)//update x angle
        cam.angle.x+=cam.speed.look*(cam.window_size.width/2.-xpos);
    if(cam.angle.y+updatey<=180.&&cam.angle.y+updatey>=0)//y angle
        cam.angle.y+=updatey;

    glm::vec3 dir((float)cos(cam.angle.y)*sin(cam.angle.x),
                  (float)sin(cam.angle.y),
                  (float)cos(cam.angle.y)*cos(cam.angle.x));
    glm::vec3 right((float)sin(cam.angle.x-cam.angle.xc/2.),
                    (float)0,
                    (float)cos(cam.angle.x-cam.angle.xc/2.));
    glm::vec3 up=cross(right,dir);
    //glm::vec3 up=vec3(1.,1.,1.);


    if(glfwGetKey(cam.window,GLFW_KEY_W)==GLFW_PRESS)
        cam.pos+=dir*delta*cam.speed.move;

    if(glfwGetKey(cam.window,GLFW_KEY_S)==GLFW_PRESS)
        cam.pos-=dir*delta*cam.speed.move;

    if(glfwGetKey(cam.window,GLFW_KEY_D)==GLFW_PRESS)
        cam.pos+=right*delta*cam.speed.move;

    if(glfwGetKey(cam.window,GLFW_KEY_A)==GLFW_PRESS)
        cam.pos-=right*delta*cam.speed.move;


    cam.matrix.projection=glm::perspective(cam.fov,cam.window_size.ratio,0.1f,1000.f);

    cam.matrix.view=glm::lookAt(cam.pos,cam.pos+dir,up);


    *_cam=cam;
}



void xcamUpdateLook(XCamera* _cam,double x_offset,double y_offset)
{
	XCamera cam;
	float updatey=cam.speed.look*y_offset;

    
        cam.angle.x+=cam.speed.look*x_offset;
    	if(cam.angle.y+updatey<=180.&&cam.angle.y+updatey>=0)//y angle
        	cam.angle.y+=updatey;

    	glm::vec3 dir((float)cos(cam.angle.y)*sin(cam.angle.x),
                  (float)sin(cam.angle.y),
                  (float)cos(cam.angle.y)*cos(cam.angle.x));
    	glm::vec3 right((float)sin(cam.angle.x-cam.angle.xc/2.),
                    (float)0,
                    (float)cos(cam.angle.x-cam.angle.xc/2.));
    	glm::vec3 up=cross(right,dir);	
	
	
	

	*_cam=cam;
	
}
