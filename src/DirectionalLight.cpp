#include "DirectionalLight.h"


DirectionalLight::DirectionalLight()
{
}

DirectionalLight::~DirectionalLight()
{
}


void DirectionalLight::init(DL_UniformLocations u)
{
	u_locs=u;
}

void DirectionalLight::initModel(Model model)
{
	quad=model;
}

void DirectionalLight::set_MVP(mat4 m)
{
	MVP=m;
}

void DirectionalLight::set_LightData(DirectionalLight_Params p)
{
	this_light=p;
}

void DirectionalLight::render(GLSLProgram shader)
{
	//set uniforms
	//u_locs
	glUniformMatrix4fv(u_locs.mvp, 1, GL_FALSE,glm::value_ptr(MVP));	
	glUniform3fv(u_locs.color,1,value_ptr(this_light.Color));
	glUniform1f(u_locs.amb_intens,this_light.AmbientIntensity);
	glUniform1f(u_locs.diff_intens,this_light.DiffuseIntensity);
	glUniform3fv(u_locs.direction,1,glm::value_ptr(this_light.Direction));
	
	quad.draw(shader);
}

