#include <GL/glew.h>
#include <GL/gl.h>


#include <stdio.h>
#include <stdlib.h>


#ifndef asfgrgh
#define asfgrgh

struct GLSLShader
{
    unsigned int id;
    GLenum type;
};

struct GLSLProgram
{
    unsigned int id;
    char* name;
};




void glslInit();

GLSLProgram glslBuildProgram(char* name_vertex,char* name_fragment);

GLSLShader glslBuildShader(char* name,GLenum type);

void glslUse(GLSLProgram program);

#endif
