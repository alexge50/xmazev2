
//Opengl
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include "GLProgram.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

//std
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#ifndef XMesh
#define XMesh

using namespace std;

struct Vertex{
	glm::vec3 position;//position on point
	glm::vec3 normal;//normal
	glm::vec2 texcoord;//texture coordinate
};

struct Texture{
	GLuint id;
	string type;
	aiString path;
};

class Mesh{
public:
/*
	Vertex* vertices;
	int n_vertices;
	GLuint* indices;
	int n_indices;
	Texture* textures;
	int n_textures;
*/

	vector<Vertex> vertices;
    	vector<GLuint> indices;
   	vector<Texture> textures;
	  
  
	Mesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> textures);
	void draw(GLSLProgram);

private: 
	GLuint vao,vbo,ebo;

	void setup_mesh();
  
};

#endif
  
