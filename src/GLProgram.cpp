#include "GLProgram.h"


/*Local Tools*/
char* file2buf(char* fname)
{
    
    //return (char*)malloc(2);
    FILE *f = fopen(fname, "rb");

    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    //long fsize=1234;

    char *string = (char*)malloc(fsize + 1);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = 0;

    return string;
}
/* */

/*Library Function Definition*/
void glslInit()
{
    if(GLEW_OK!=glewInit())
        printf("ERROR (GLSL Program): An error has occured while initing GLEW (fail)\n"),exit(-1);

    if(GLEW_ARB_vertex_shader && GLEW_ARB_fragment_shader)
        ;
    else
        printf("ERROR (GLSL Program): Extensions for vertex & fragment shader are not supported\n"),exit(-1);

    if (!glewIsSupported("GL_VERSION_2_0"))
		printf("ERROR (GLSL Program): OpenGL 2.0 not supported. If you don't have lattest drivers on your machine please install\n");

}


GLSLShader glslBuildShader(char* name,GLenum type)
{
    GLSLShader shader;
    char* _filebuf=file2buf(name);
    const char* filebuf=_filebuf;


    shader.id=glCreateShader(type);
    glShaderSource(shader.id,1,&filebuf,NULL);
    glCompileShader(shader.id);

    shader.type=type;

	char compile[10000+1];
	int length;
	
	printf("Error log in %s:\n",name);

	glGetShaderInfoLog(shader.id,10000,&length,compile);

	compile[length+1]='\0';
		
	printf("%s\n",compile);

    free(_filebuf);

    return shader;
}

GLSLProgram glslBuildProgram(char* name_vertex,char* name_fragment)
{
    GLSLShader vertex,fragment;
    GLSLProgram program;
    int err;

    vertex=glslBuildShader(name_vertex,GL_VERTEX_SHADER);
    fragment=glslBuildShader(name_fragment,GL_FRAGMENT_SHADER);

    program.id=glCreateProgram();

    glAttachShader(program.id,vertex.id);
    glAttachShader(program.id,fragment.id);

    glLinkProgram(program.id);

    glGetObjectParameterivARB(program.id,GL_OBJECT_LINK_STATUS_ARB,&err);

    if(!err){
     printf("WARNING (GLSL Program): Couldn't link program. The Shader Program will not be used. The Fixed-Function will do drawing \n failed in:%s %s ",name_vertex,name_fragment);
	}

    return program;
}

void glslUse(GLSLProgram program)
{
    glUseProgram(program.id);
}
