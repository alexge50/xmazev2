#include "PointLight.h"

#define max(a,b) ((a)>(b)?(a):(b))


//utility
/*
float CalcPointLightBSphere(const PointLight_Params Light)//distance scale for a point light
{
    float MaxChannel = max(max(Light.Color.x, Light.Color.y), Light.Color.z);

    float ret = (-Light.Attenuation.Linear + sqrtf(Light.Attenuation.Linear * Light.Attenuation.Linear -
        4 * Light.Attenuation.Exp * (Light.Attenuation.Exp - 256 * MaxChannel * Light.DiffuseIntensity)))
            /
        2 * Light.Attenuation.Exp;
    return ret;
} 
*/
//

PointLight::PointLight()
{
	//Model sph("sphere.obj");
	//sphere=sph;
}

PointLight::~PointLight()
{
	
}

void PointLight::init(PL_UniformLocations u)
{
	u_locs=u;
}

void PointLight::initModel(Model model)
{
	sphere=model;
}

void PointLight::set_MVP(mat4 m)
{
	MVP=m;
}

void PointLight::set_LightData(PointLight_Params p)
{
	this_light=p;
}

void PointLight::render(GLSLProgram shader)
{
	//set uniforms
	//u_locs
	glUniformMatrix4fv(u_locs.mvp, 1, GL_FALSE,glm::value_ptr(MVP));	
	glUniform3fv(u_locs.color,1,value_ptr(this_light.Color));
	glUniform1f(u_locs.amb_intens,this_light.AmbientIntensity);
	glUniform1f(u_locs.diff_intens,this_light.DiffuseIntensity);
	glUniform3fv(u_locs.position,1,glm::value_ptr(this_light.Position));
	glUniform1f(u_locs.atten_const,this_light.Atten.Constant);
	glUniform1f(u_locs.atten_lin,this_light.Atten.Linear);
	glUniform1f(u_locs.atten_exp,this_light.Atten.Exp);	

	sphere.draw(shader);
}



