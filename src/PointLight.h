#include <GL/glew.h>
#include "Model.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "GLProgram.h"


using namespace glm;


typedef  struct {
        vec3 Color;
        float AmbientIntensity;
        float DiffuseIntensity;
        vec3 Position;
        struct {
            float Constant;
            float Linear;
            float Exp;
        } Atten;
    } PointLight_Params; 

typedef struct {
    GLuint mvp;
	GLuint color;
	GLuint amb_intens;
	GLuint diff_intens;
	GLuint position;
	GLuint atten_const;
	GLuint atten_lin;
	GLuint atten_exp; 
    }PL_UniformLocations;
  

class PointLight
{
public:
	PointLight_Params this_light;
	mat4 MVP;

	 //PointLight(PointLight_Params p,GLSLProgram s);
	 PointLight();
	~PointLight();
	
	void init(PL_UniformLocations u);
	void initModel(Model model);
	void set_MVP(mat4 m);
	void set_LightData(PointLight_Params p);
	void render(GLSLProgram s);

	
	//void init(PointLight_Params p,GLSLProgram s);
	//void init_UniformLoc(UniformLocations u);
	//void update_LightData(Point_LightParams);
	//void update_mvp();//recalculate mvp transformation
	//void render();

private:

	PL_UniformLocations u_locs;
	Model sphere;
		
}; 
