#include "FT_text.h"


ft_char::ft_char(GLuint id,ivec2 _size,ivec2 _bearing,GLuint _advance)
{
    id_texture=id;
    size=_size;
    bearing=_bearing;
    advance=_advance;
}
ft_char::ft_char()
{
}

xText::xText()
{
}

xText::xText(char* f_font,int width,int height)
{
    FT_Library ft;
    FT_Face face;
    unsigned char c;

    if(FT_Init_FreeType(&ft))
    {
        printf("Error (FreeType): Couldn't init FreeType\n");
        return ;
    }

    if(FT_New_Face(ft,f_font,0,&face))
    {
        printf("Error (FreeType): Couldn't load font\n");
        return ;
    }

    FT_Set_Pixel_Sizes(face,width,height);

    glPixelStorei(GL_UNPACK_ALIGNMENT,1);

    for(c=32;c<=127;c++)
    {
        printf("here1 '%c'\n",c);
        if(FT_Load_Char(face,c,FT_LOAD_RENDER))
        {
            printf("Error (FreeType): Couldn't load glyph '%c'\n",c);
        }
        else
        {
            GLuint texture;
            glGenTextures(1,&texture);
            glBindTexture(GL_TEXTURE_2D,texture);
            glTexImage2D(
                GL_TEXTURE_2D,
                0,
                GL_RED,
                face->glyph->bitmap.width,
                face->glyph->bitmap.rows,
                0,
                GL_RED,
                GL_UNSIGNED_BYTE,
                face->glyph->bitmap.buffer
            );
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            map[c]=ft_char(
                           texture,
                           ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                           ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                           face->glyph->advance.x
                          );
        }
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    FT_Done_Face(face);
    FT_Done_FreeType(ft);

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void xText::print(GLSLProgram s,mat4 MVP,char* text,float x,float y,float scale,vec3 color)
{
    int n_text=strlen(text);
    int i;

    glslUse(s);

    glUniform3f(glGetUniformLocation(s.id, "textColor"), color.x, color.y, color.z);
    glUniformMatrix4fv(glGetUniformLocation(s.id, "MVP"), 1, GL_FALSE, glm::value_ptr(MVP));

    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(vao);

    for(i=0;i<n_text;i++)
    {
        ft_char c=map[text[i]];

        GLfloat xpos = x + c.bearing.x * scale;
        GLfloat ypos = y - (c.size.y - c.bearing.y) * scale;

        GLfloat w = c.size.x * scale;
        GLfloat h = c.size.y * scale;
        // Update VBO for each character
        GLfloat vertices[6][4] = {
            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 }
        };
        // Render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, c.id_texture);
        glUniform1i(glGetUniformLocation(s.id, "text"),0);
        // Update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // Be sure to use glBufferSubData and not glBufferData
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (c.advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
    }

    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}
