#include "Mesh.h"
#define OFFSETOF(st, m) ((size_t)(&((st *)0)->m))


Mesh::Mesh(vector<Vertex> a, vector<GLuint> b, vector<Texture> c)
{
    vertices=a;
    indices=b;
    textures=c;

    setup_mesh();
}

void Mesh::draw(GLSLProgram shader)
{

    // Bind appropriate textures
    GLuint diffuseNr = 1;
    GLuint specularNr = 1;
    for(GLuint i = 0; i < this->textures.size(); i++)
    {
        glActiveTexture(GL_TEXTURE0 + i); // Active proper texture unit before binding
        // Retrieve texture number (the N in diffuse_textureN)
        stringstream ss;
        string number;
        string name = this->textures[i].type;
        if(name == "texture_diffuse")
            ss << diffuseNr++; // Transfer GLuint to stream
        else if(name == "texture_specular")
            ss << specularNr++; // Transfer GLuint to stream
        number = ss.str();
        // Now set the sampler to the correct texture unit
        glUniform1f(glGetUniformLocation(shader.id ,(name + number).c_str()), i);
        // And finally bind the texture
        glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
    }

    // Also set each mesh's shininess property to a default value (if you want you could extend this to another mesh property and possibly change this value)
    glUniform1f(glGetUniformLocation(shader.id, "material.shininess"), 16.0f);

    // Draw mesh
    glBindVertexArray(this->vao);
    glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // Always good practice to set everything back to defaults once configured.
    for (GLuint i = 0; i < this->textures.size(); i++)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, 0);
    }


//	GLuint n_diff=1,n_spec=1;
//	GLuint i;
//	char* name,shader_name;
//
//	for(i=0;i<n_textures;i++)
//	{
//		glActiveTexture(GL_TEXTURE0+i);
//		name=textures[i].type;
//
//		if(!strcmp(name,"texture_diffuse"))
//			sprintf(shader_name,"%s%d",name,n_diff++);
//		if(!strcmp(name,"texture_specular"))
//			sprintf(shader_name,"%s%d",name,n_spec++);
//
//		glUniform1f(glGetUniformLocation(shader.id,shader_name),i);
//
//		glBindTexture(GL_TEXTURE_2D,textures[i].id)
//
//	}
//
//	 glUniform1f(glGetUniformLocation(shader.id, "material.shininess"), 16.0f);
//
//	glBindVertexArray(vao);
//	glDrawElements(GL_TRIANGLES,n_indices,GL_UNSIGNED_INT,0);
//	glBindVertexArray(0);
}

void Mesh::setup_mesh()
{
    // Create buffers/arrays
        glGenVertexArrays(1, &vao);
        glGenBuffers(1, &vbo);
        glGenBuffers(1, &ebo);

        glBindVertexArray(vao);

        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);


        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));

        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texcoord));

        glBindVertexArray(0);
}

/*
class Mesh{
public:
	Vertex* vertices;
	int n_vertices;
	GLuint* indices;
	int n_indices;
	Texture* textures;
	int n_textures;


	Mesh(Vertex*,int,GLuint*,int,Texture*,int);
	void Draw(GLSLProgram);

private:
	GLuint vao,vbo,ebo;

	void setup_mesh();

};

*/
