#include "game_logic.h"

#define MAX_CLOCK 	126
#define MONSTER_SHIELD 25


using namespace xmaze;


//struct point
//{
//    int l,c;
//    point();
//    point(int _l,int _c);
//};


point::point()
{
}

point::point(int a,int b)
{
    l=a;
    c=b;
}

void point::setpoint(int _l,int _c)
{
    l=_l;
    c=_c;
}

Light::Light()
{
    active=1;
}
Light::Light(float _r,float _g,float _b,int _l,int _c)
{
    r=_r;
    g=_g;
    b=_b;
    l=_l;
    c=_c;
    active=1;
}


logic::logic()
{
    life=5;
    score=0;
    done=1;
}

char logic::getInfo(int l,int c)
{
    return table[l][c][0];
}
int logic::getLife()
{
    return life;
}

char logic::isDone()
{
    return done;
}

int logic::getScore()
{
    return score;
}


Light logic::light(int i)
{
    return light_index[i];
}
int logic::n_lights()
{
    return v_n_lights;
}

void logic::update(char *keys)
{
    char feedback[4][5]=
    {
        {'W','w',+1,+0,'^'},
        {'S','s',-1,+0,'V'},
        {'A','a',+0,-1,'<'},
        {'D','d',+0,+1,'>'}
    };

    char laser_dirl[128],
         laser_dirc[128];

    laser_dirl['>']= 0;
    laser_dirc['>']= 1;

    laser_dirl['<']= 0;
    laser_dirc['<']=-1;

    laser_dirl['^']= 1;
    laser_dirc['^']= 0;

    laser_dirl['V']=-1;
    laser_dirc['V']= 0;


    int i,j;
    int k;


    char died=0;
    for(i=0; i<TABLE_HEIGHT; i++)
    {
        for(j=0; j<TABLE_WIDTH; j++)
        {
            if(table[i][j][0]=='>'||table[i][j][0]=='<'||table[i][j][0]=='^'||table[i][j][0]=='V')
            {
                if(table[i][j][1]!=clock)
                {
                    char moved=0;
                    table[i][j][1]=clock;
                    for(k=0; k<4; k++)
                    {
                        if(keys[feedback[k][0]]||keys[feedback[k][1]])
                        {
                            if(table[feedback[k][2]+i][feedback[k][3]+j][0]==SPACE&&!moved)
                            {
                                table[feedback[k][2]+i][feedback[k][3]+j][0]=feedback[k][4],table[i][j][0]=SPACE;
                                table[feedback[k][2]+i][feedback[k][3]+j][1]=table[i][j][1];
                                entity.setpoint(feedback[k][2]+i,feedback[k][3]+j);
                                entity.p_init=table[feedback[k][2]+i][feedback[k][3]+j][0];
                                moved=1;
                            }
                            else if(table[feedback[k][2]+i][feedback[k][3]+j][0]=='M'||table[feedback[k][2]+i][feedback[k][3]+j][0]=='L')
                                died=1;
                            else if(table[feedback[k][2]+i][feedback[k][3]+j][0]=='@')
                                done=1,score+=1000;
                        }
                    }
                }
            }
            else if(table[i][j][0]=='M'||table[i][j][0]=='L')//monsters
            {
                if(table[i][j][3]==0){
                        if(table[i][j][0]=='L')
                               light_index[table[i][j][5]].active=0,printf("here4556\n");
                        table[i][j][0]=SPACE;
                        score+=100;
                    }
                else
                {
                    if(clock%table[i][j][4]==0)
                    {
                        if(table[i+table[i][j][1]][j+table[i][j][2]][0]==SPACE&&clock!=table[i][j][6])
                        {
                            int k;
                            int ldir=table[i][j][1];
                            int cdir=table[i][j][2];
                            for(k=0; k<DEPTH; k++)
                            {
                                table[i+ldir][j+cdir][k]=table[i][j][k];
                                table[i][j][k]=0;
                            }
                            table[i][j][0]=SPACE;
                            table[i+ldir][j+cdir][6]=clock;
                            if(table[i+ldir][j+cdir][0]=='L')
                            {
                                light_index[table[i+ldir][j+cdir][5]].l=i+ldir;
                                light_index[table[i+ldir][j+cdir][5]].c=j+cdir;
                            }
                            //printf("%d %d (%d %d) %c\n",table[i+ldir][j+cdir][1],table[i+ldir][j+cdir][2],i+ldir,j+cdir,table[i+ldir][j+cdir][0]);
                        }
                        else if(table[i+table[i][j][1]][j+table[i][j][2]][0]=='<'||
                                table[i+table[i][j][1]][j+table[i][j][2]][0]=='^'||
                                table[i+table[i][j][1]][j+table[i][j][2]][0]=='>'||
                                table[i+table[i][j][1]][j+table[i][j][2]][0]=='V')
                            died=1;
                        else if(table[i+table[i][j][1]][j+table[i][j][2]][0]=='#')
                        {
                            table[i+table[i][j][1]][j+table[i][j][2]][1]=table[i][j][1];
                            table[i+table[i][j][1]][j+table[i][j][2]][2]=table[i][j][2];
                            table[i][j][1]*=(-1);
                            table[i][j][2]*=(-1);
                            table[i+table[i][j][1]][j+table[i][j][2]][1]*=-1;
                            table[i+table[i][j][1]][j+table[i][j][2]][2]*=-1;
                            //printf("%d %d (%d %d) %c\n",table[i][j][1],table[i][j][2],i,j,table[i][j][0]);
                        }
                        else if(table[i+table[i][j][1]][j+table[i][j][2]][0]=='X')//laser
                        {
                            table[i][j][3]--;
                        }

                    }
                }
            }
            else if(table[i][j][0]=='X')//laser
            {
                if(table[i][j][3]!=clock)
                {
                    if(table[i+table[i][j][1]][j+table[i][j][2]][0]==' ')
                    {
                        int k;
                        int ldir=table[i][j][1];
                        int cdir=table[i][j][2];
                        for(k=0; k<DEPTH; k++)
                        {
                            table[i+ldir][j+cdir][k]=table[i][j][k];
                            table[i][j][k]=0;
                        }
                        table[i][j][0]=SPACE;
                        table[i+ldir][j+cdir][3]=clock;
                    }
                    else if(table[i+table[i][j][1]][j+table[i][j][2]][0]=='M'||
                            table[i+table[i][j][1]][j+table[i][j][2]][0]=='L')
                    {
                        if(table[i+table[i][j][1]][j+table[i][j][2]][3]!=MONSTER_SHIELD)
                            table[i+table[i][j][1]][j+table[i][j][2]][3]--;//minus life for hitted monster
                    }
                    else if(table[i+table[i][j][1]][j+table[i][j][2]][0]=='#'||
                            table[i+table[i][j][1]][j+table[i][j][2]][0]=='@')
                    {
                        table[i][j][0]=SPACE;
                    }
                    table[i][j][3]=clock;
                }
            }
        }
    }

    if(died)
    {
        for(i=0; i<TABLE_HEIGHT; i++)
        {
            for(j=0; j<TABLE_WIDTH; j++)
            {
                if(table[i][j][0]=='>'||table[i][j][0]=='<'||table[i][j][0]=='V'||table[i][j][0]=='^')
                    table[i][j][0]=SPACE;
            }
        }
        table[character_start.l][character_start.c][0]=character_start.p_init;
        life--;
        score-=10;
    }

    if(keys[' '])//lasers launch--1 by frame
    {
        if(table[laser_dirl[entity.p_init]+entity.l][laser_dirc[entity.p_init]+entity.c][0]==SPACE)//"free" launch
        {
            table[laser_dirl[entity.p_init]+entity.l][laser_dirc[entity.p_init]+entity.c][0]='X';
            table[laser_dirl[entity.p_init]+entity.l][laser_dirc[entity.p_init]+entity.c][1]=laser_dirl[entity.p_init];
            table[laser_dirl[entity.p_init]+entity.l][laser_dirc[entity.p_init]+entity.c][2]=laser_dirc[entity.p_init];
            table[laser_dirl[entity.p_init]+entity.l][laser_dirc[entity.p_init]+entity.c][3]=clock;
        }
        else if(table[laser_dirl[entity.p_init]+entity.l][laser_dirc[entity.p_init]+entity.c][0]=='M'||
                table[laser_dirl[entity.p_init]+entity.l][laser_dirc[entity.p_init]+entity.c][0]=='L')
        {
            if(table[laser_dirl[entity.p_init]+entity.l][laser_dirc[entity.p_init]+entity.c][3]!=MONSTER_SHIELD)
                table[laser_dirl[entity.p_init]+entity.l][laser_dirc[entity.p_init]+entity.c][3]--;//minus life for hitted monster
        }

    }


    clock++;
    if(clock>MAX_CLOCK)
    {
        clock=0;
    }
}

void logic::newlevel(char* fname)
{
    clock=0;
    done=0;
    loadlevel(fname);
}

void logic::loadlevel(char* fname)
{
    FILE *fin=fopen(fname,"r");
    int i,j;
    int l,c;
    int n;


    for(i=0; i<TABLE_HEIGHT; i++)
    {
        for(j=0; j<TABLE_WIDTH; j++)
        {
            table[i][j][0]=fgetc(fin);
            if(table[i][j][0]=='>'||table[i][j][0]=='<'||table[i][j][0]=='V'||table[i][j][0]=='^')
            {
                character_start.setpoint(i,j);
                character_start.p_init=table[i][j][0];
            }
//            else if(table[i][j][0]=='X')
//            {
//                table[i][j][1]=0;
//                table[i][j][2]=1;
//                table[i][j][3]=clock;
//            }
        }
        fgetc(fin);
    }

    fscanf(fin,"%d",&n);//monstrii normali
    {

        for(i=0; i<n; i++)
        {
            int dirl,dirc,life,speed;
            fscanf(fin,"%d %d %d %d %d %d",&l,&c,&dirl,&dirc,&life,&speed);
            l--;
            c--;
            table[l][c][0]='M';
            table[l][c][1]=dirl;
            table[l][c][2]=dirc;
            table[l][c][3]=life;
            table[l][c][4]=speed;
        }
    }


    fscanf(fin,"%d",&n);//monstrii luminosi
    {
        light_index=(Light*)malloc(sizeof(Light)*n);
        v_n_lights=n;
        for(i=0; i<n; i++)
        {
            int dirl,dirc,life,speed;
            float r,g,b;
            fscanf(fin,"%d %d %d %d %d %d %f %f %f",&l,&c,&dirl,&dirc,&life,&speed,&r,&g,&b);
            l--;
            c--;
            table[l][c][0]='L';
            table[l][c][1]=dirl;
            table[l][c][2]=dirc;
            table[l][c][3]=life;
            table[l][c][4]=speed;
            table[l][c][5]=i;
            table[l][c][6]=clock;
            light_index[i]=Light
            (
                r,g,b,l,c
            );
        }
    }

    fscanf(fin,"%d",&n);
    isDirectional=n;

    fclose(fin);
}

void logic::getMonsterOrientation(int*l,int*c,int l_now,int c_now)
{
    *(l)=table[l_now][c_now][1];
    *(c)=table[l_now][c_now][2];
}

/*
Normal:
0:tabla
1:dirl
2:dirc
3:life
4:speed
6:last modified clock
(Light):
5:light index
*/

/*
class logic{

public:
    char getInfo(int l, int c);
    void update(char *keys);
    void new_level();


private:
    char table[TABLE_HEIGHT][TABLE_WIDTH][DEPTH];
    int life,score;
    long long int clock;
    void loadlevel(char*);

};
*/
