#include <GL/glew.h> 
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SOIL/SOIL.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>

#include <stdio.h>
#include <stdlib.h>

#include "Mesh.h"


#ifndef qwert
#define qwert

using namespace std;

GLuint TextureFromFile(const char *path,string directory);

class Model
{
public:
	Model(char* path);
	Model();
	void draw(GLSLProgram shader);
	
	void loadModel(string path);//public

private:
	vector<Mesh> meshes;
	string directory;
	vector<Texture> textures_loaded;
	
	//void loadModel(string path);
	void processNode(aiNode* node,const aiScene* scene);
	Mesh processMesh(aiMesh* mesh,const aiScene* scene);
	vector<Texture> loadMaterialTextures(aiMaterial* mat,aiTextureType type, string type_name);
	
};

#endif
