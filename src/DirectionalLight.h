#include <GL/glew.h>
#include "Model.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "GLProgram.h"


using namespace glm;


typedef struct {
	vec3 Color;
	float AmbientIntensity;
    float DiffuseIntensity;
	vec3 Direction;
	
	void DirectionalLight_Params()
	{
		Color=vec3(0.,0.,0.);
		AmbientIntensity=0.;
		AmbientIntensity=0.;
		Direction=vec3(0.,0.,0.);
	} 

}DirectionalLight_Params;


typedef struct {
	GLuint mvp,color,amb_intens,diff_intens,direction;	
}DL_UniformLocations;



class DirectionalLight
{
public:
	DirectionalLight_Params this_light;
	mat4 MVP;
	
	 DirectionalLight();
	~DirectionalLight();

	void init(DL_UniformLocations u);
	void initModel(Model m);
	void set_MVP(mat4 m);
	void set_LightData(DirectionalLight_Params);
	void render(GLSLProgram shader);
private:
	DL_UniformLocations u_locs;
	Model quad;
};
