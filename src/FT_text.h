#include <GL/glew.h>
#include "GLProgram.h"
#include <GL/gl.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stdio.h>
#include <string.h>


using namespace glm;

struct ft_char
{
    GLuint id_texture;
    ivec2 size,bearing;
    GLuint advance;

    ft_char(GLuint id,ivec2 size,ivec2 bearing,GLuint advance);
    ft_char();
};


class xText
{
public:
    xText(char* font,int w,int h);
    xText();

    void print(GLSLProgram,mat4 MVP,char* text,float x,float y,float scale,vec3 color);;

private:
    ft_char map[128];
    GLuint vao,vbo;
};
