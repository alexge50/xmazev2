#include <GL/glew.h>
#include <GL/gl.h>


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ZERO_MEM(a) memset(a, 0, sizeof(a))


#define GBUFFER_TEXTURE_NUM 3


enum GBuffer_texture_type
{
    GBuffer_position,
    GBuffer_diffuse,
    GBuffer_normal
};

class GBuffer
{
public:
     GBuffer();
    ~GBuffer();

    char init(int width,int heigth);

    void bind_write();
    void bind_read();
    void set_read(GBuffer_texture_type TextureType);
private:
    GLuint id_fbo;
    GLuint id_tex[GBUFFER_TEXTURE_NUM];
    GLuint id_depthtex;
};
