#version 330




struct DirLight
{
    vec3 Color;
    float AmbientIntensity;
    float DiffuseIntensity;
    vec3 Direction;
};

//gbuffer data
uniform sampler2D PositionMap;
uniform sampler2D ColorMap;
uniform sampler2D NormalMap;

//light data
uniform DirLight dirlight;

//other
uniform vec3 EyeWorldPos;
uniform float MatSpecularIntensity;
uniform float SpecularPower;
uniform vec2 ScreenSize;



vec4 calcLightInternal(DirLight dl, vec3 LightDirection, vec3 Position, vec3 Normal)
{
	vec4 amb_color=vec4(dl.Color,1.0)*dl.AmbientIntensity;
	float diff_factor=dot(Normal,-LightDirection);
   
    vec4 diff_color=vec4(0,0,0,0);
    vec4 spec_color=vec4(0,0,0,0);
  
    if(diff_factor>0.0)
    {
		diff_color=vec4(dl.Color,1.0)*dl.DiffuseIntensity*diff_factor;
		
		vec3 VertexToEye =normalize(EyeWorldPos-Position);
		vec3 LightReflect=normalize(reflect(LightDirection,Normal));

		float spec_factor=dot(VertexToEye,LightReflect);
		spec_factor=pow(spec_factor,SpecularPower);
		

		if(spec_factor>0.0)
		{
			spec_color=vec4(dl.Color,1.0)*MatSpecularIntensity*spec_factor;
		}
    }
	
	
	return  (amb_color+diff_color+spec_color);
}


vec4 calcDirLight(vec3 Position, vec3 Normal)
{
	return calcLightInternal(dirlight,dirlight.Direction,Position,Normal);
}


vec2 gbufferTexCoord()
{
    return gl_FragCoord.xy/ScreenSize;
}

out vec4 FragColor;

void main()
{
    vec2 gbuffer_texcoord=gbufferTexCoord();
    
    vec3 Position=texture(PositionMap,gbuffer_texcoord).xyz;
    vec3 Color   =texture(ColorMap   ,gbuffer_texcoord).xyz;
    vec3 Normal  =texture(NormalMap  ,gbuffer_texcoord).xyz;
	//FragColor=vec4(Color,1);
	FragColor=vec4(Color,1.0)*calcDirLight(Position,Normal);
}
