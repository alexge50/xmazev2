#version 330

in vec2 tc;
uniform sampler2D _tex;
out vec4 color;

void main()
{
	vec3 tex=texture2D(_tex,tc).rgb;
    //gl_FragColor =vec4(tex,1);
	color=vec4(tex,1);
} 
