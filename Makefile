#CC=g++
#CFLAGS=-Wall -c
#LDFLAGS=-lglfw3 -lGL -lGLU -lX11 -lXxf86vm -lXrandr -lpthread -lXi -lGLEW -lSOIL
#SOURCES=src/main.cpp src/Input.cpp src/Camera.cpp
#OBJECTS=lib/main.o src/Input.o src/Camera.o
#EXECUTABLE="bin/alpha mapping"

#OBJ=$(SOURCES : .cpp=.o)

#all: $(SOURCES) $(EXECUTABLE)

#$(EXECUTABLE): $(OBJECTS)
#	$(CC) $(CFLAGS) $(OBJECTS)  -o $(EXECUTABLE) $(LDFLAGS) 
#	echo $(OBJ)

#.cpp.o:
	#$(CC) $(CFLAGS) $(SOURCES)  -o $(OBJECTS) 
#	#$(CC) $(CFLAGS) $< -o $@
#	@for x in $(OBJECTS);\ 
#	do \
#		$CC $(CFLAGS) $< $$x

CC = g++
SRC = src/main.cpp src/GLProgram.cpp src/Model.cpp src/Mesh.cpp src/camera.cpp src/GBuffer.cpp src/PointLight.cpp src/DirectionalLight.cpp src/game_logic.cpp src/FT_text.cpp
#OBJ = lib/main.o lib/Input.o lib/Camera.o
EXEC = "./bin/XMaze"
LIBS= -lGL -lGLU -lglfw -lX11 -lXxf86vm -lXrandr -lpthread -lXi -lXinerama -lXcursor -lSOIL -lGLEW -lassimp -lfreetype -lm -ldl
CFLAGS= -I/usr/include/freetype2
RUN = true
#LIBDIR=lib/
#SRCDIR=src/
#ROOTDIR=../

all:build run

build: $(SRC) 
	#cd $(SRCDIR)
	#echo lokk creagt
	$(CC) $(CFLAGS) -o $(EXEC) $(SRC) $(LIBS)
	#$(EXEC)
	#cd $(ROOTDIR)
	#cd $(LIBDIR)
	#$(CC) -o $(OBJ) $(SRC) $(LIBS)
	#run
	#ifeq($(RUN),true)
		#$(EXEC)

clean:
	rm -f *.o core

rebuild: clean build

run: 
	$(EXEC)
