#version 330 core

in vec2 TexCoords0;
in vec3 Normal0;
in vec3 Position0;

layout (location = 0) out vec3 WorldPosOut;
layout (location = 1) out vec3 DiffuseOut;
layout (location = 2) out vec3 NormalOut;
layout (location = 3) out vec3 TexCoordOut;


uniform sampler2D texture_diffuse1;

void main()
{    
    WorldPosOut = Position0;
    DiffuseOut = texture(texture_diffuse1, TexCoords0).xyz/*vec3(1,1,1)*/;
    NormalOut = normalize(Normal0);
    TexCoordOut = vec3(TexCoords0, 0.0);
}
